import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router'
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-actionbar',
  templateUrl: './actionbar.component.html',
  styleUrls: ['./actionbar.component.scss']
})
export class ActionbarComponent implements OnInit {
  canGoBack: boolean = false;

  blockedRoutes: string[] = ["/registration", "/catalogue"];

  previousRoutes: string[] = [];
  currentRoute: string = null;
  
  constructor(private router: Router, private auth: AuthenticationService) {
    this.routeChange(router);
  }

  get isAuthenticated() : boolean {
    return this.auth.isAuthenticated();
  }

  routeChange(router: Router) {
    this.currentRoute = router.url;
    console.log(this.currentRoute);
    router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.probeChange(e);
      }
    })
  }

  probeChange(navigation: NavigationEnd) {
    if (this.blockedRoutes.includes(navigation.urlAfterRedirects)) {
      this.canGoBack = false;
    } else {
      this.canGoBack = true;
    }
  }

  goBack() {
    window.history.back();
  }

  gotoCollection() {
    this.router.navigateByUrl('/collection');
  }

  ngOnInit(): void {
  }

}
