import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  @Input() navTitle: string;

  constructor(private router: Router, private auth: AuthenticationService) {

  }

  get isAuthenticated() : boolean {
    return this.auth.isAuthenticated();
  }

  OnLogoClick() {
    this.router.navigateByUrl('/');
  }

  OnLogoutClick() {
    this.auth.logout();
    this.router.navigateByUrl('/');
  }
}
