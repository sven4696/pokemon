import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import {Observable, BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private authUser$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(private storage: StorageService) {
    const savedUser = this.storage.get('authUser');

    if (savedUser) {
      this.authUser$.next(savedUser);
    }
  }

  isAuthenticated() : boolean {
    return Boolean(this.authUser$.value);
  }

  getAuthenticatedUser() : Observable<string> {
    return this.authUser$.asObservable();
  }

  register(user: string) {
    this.authUser$.next(user);
    this.storage.set('authUser', user);
  }

  logout() {
    this.authUser$.next('');
    this.storage.clear();
  }
}
