import { Injectable } from '@angular/core';
import { Pokemon, PokemonShort } from '../models/app.pokemon';
import { PokemonApiService } from './pokemon.api.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {
  constructor(private pokemonService: PokemonApiService, private storage: StorageService) { }

  getCollection() : PokemonShort[] {
    let collectionString: string = this.storage.get('collection');
    let collection: PokemonShort[] = JSON.parse(collectionString) || [];

    return collection;
  }

  contains(pokemon: Pokemon) : boolean {
    return this.getCollection().filter(p => p.name == pokemon.name).length == 0;
  }

  add(pokemon: Pokemon) {
    let collection: PokemonShort[] = this.getCollection();
    let dto: PokemonShort = this.pokemonService.getPokemonDto(pokemon);

    if (collection.filter(pokemon => pokemon.name == dto.name).length == 0) {
      collection.push(dto);
      this.save(collection);
    }
  }

  remove(pokemon: Pokemon) {
    let altered: PokemonShort[] = this.getCollection().filter(entity => entity.name != pokemon.name);
    this.save(altered);
  }

  private save(alteredCollection: PokemonShort[]) {
    let jsonString = JSON.stringify(alteredCollection);
    this.storage.set('collection', jsonString);
    
  }
}
