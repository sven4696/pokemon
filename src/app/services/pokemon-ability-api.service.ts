import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Pokemon, PokemonAbility, PokemonAbilitiesArray, PokemonAbilityDTO } from '../models/app.pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonAbilityApiService {

  constructor(private http: HttpClient) {

  }

  public getPokemonAbilities(pokemon: Pokemon) : PokemonAbility[] {
    let abilities: PokemonAbility[] = [];

    // Get the information
    pokemon.abilities.forEach(abilityArray => {
      let abilityDto: PokemonAbilityDTO = abilityArray.ability;

      this.http.get<PokemonAbility>(abilityDto.url)
      .subscribe(ability => {
        ability.effect_entries = ability.effect_entries.filter(entry => entry.language.name == "en");

        abilities.push(ability);
      })
    })

    return abilities
  }
}
