import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Pokemon, PokemonMove, PokemonMovesArray, PokemonMoveDTO } from '../models/app.pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonMovesApiService {

  constructor(private http: HttpClient) {

  }

  public getPokemonMoves(pokemon: Pokemon) : PokemonMove[] {
    let moves: PokemonMove[] = [];

    pokemon.moves.forEach(moveArray => {
      let moveDto: PokemonMoveDTO = moveArray.move;

      this.http.get<PokemonMove>(moveDto.url)
      .subscribe(move => {
        move.effect_entries = move.effect_entries.filter(entry => entry.language.name == "en");
        moves.push(move);
      })
    });

    return moves;
  }
}
