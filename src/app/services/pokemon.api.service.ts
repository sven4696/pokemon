import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pokemon, PokemonList, PokemonShort } from '../models/app.pokemon';
import { PokemonDetailComponent } from '../views/details/pokemon-detail/pokemon-detail.component';

@Injectable({
  providedIn: 'root'
})
export class PokemonApiService {
  private pokemonFetchLimit: number = 850;

  constructor(private http: HttpClient) { }

  public getPokemons() : Observable<PokemonList> {
    return this.http.get<PokemonList>(`https://pokeapi.co/api/v2/pokemon/?limit=${this.pokemonFetchLimit}`);
  }

  public getPokemon(id: number) : Observable<Pokemon> {
    return this.http.get<Pokemon>(`https://pokeapi.co/api/v2/pokemon/${id}`);
  }

  public getPokemonDetails(pokemon: Pokemon) : Observable<Pokemon> {
    return this.http.get<Pokemon>(pokemon.url);
  }

  public getPokemonDto(pokemon: Pokemon) : PokemonShort {
    let dto: PokemonShort = {
      name: pokemon.name,
      url: `https://pokeapi.co/api/v2/pokemon/${pokemon.id}`
    }

    return dto;
  }
}
