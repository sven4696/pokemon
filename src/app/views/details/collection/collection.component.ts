import { Component, OnInit } from '@angular/core';
import { Pokemon, PokemonShort } from 'src/app/models/app.pokemon';
import { CollectionService } from 'src/app/services/collection.service';
import { PokemonApiService } from 'src/app/services/pokemon.api.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {
  pokemons: Array<Pokemon> = [];

  constructor(private collectionService: CollectionService, private pokemonService: PokemonApiService) {
    let collection: Array<PokemonShort> = collectionService.getCollection();

    collection.forEach((pokemonDto: PokemonShort) => {
      let splitUrl = pokemonDto.url.split('/');
      let id = Number(splitUrl[splitUrl.length - 1]);

      this.pokemonService.getPokemon(id)
      .subscribe(pokemon => {
        this.pokemons.push(pokemon);
      });
    })
  }

  ngOnInit(): void {
  }

}
