import { Component, OnInit, Input, ɵCompiler_compileModuleSync__POST_R3__ } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { PokemonApiService } from '../../../services/pokemon.api.service';
import { Pokemon, PokemonAbility, PokemonAbilityDTO, PokemonList, PokemonMove, PokemonSprite } from 'src/app/models/app.pokemon';
import { PokemonAbilityApiService } from 'src/app/services/pokemon-ability-api.service';
import { CollectionService } from 'src/app/services/collection.service';
import { PokemonMovesApiService } from 'src/app/services/pokemon-moves-api.service';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.scss']
})
export class PokemonDetailComponent implements OnInit {
  Id: number = null;
  Pokemon: Pokemon = null;
  Abilities: Array<PokemonAbility> = [];
  Moves: Array<PokemonMove> = [];

  MoveTableColumns: string[] = ['name'];

  constructor(
      private route: ActivatedRoute,
      private pokemonService: PokemonApiService,
      private pokemonAbilityService: PokemonAbilityApiService,
      private pokemonMovesService: PokemonMovesApiService,
      private pokemonCollectionService: CollectionService) {

    this.Id = Number(route.snapshot.params.id);

    this.pokemonService.getPokemon(this.Id).subscribe(pokemon => {
      this.Pokemon = pokemon;

      // Get abilities
      this.Abilities = pokemonAbilityService.getPokemonAbilities(this.Pokemon);
      this.Moves = pokemonMovesService.getPokemonMoves(this.Pokemon);
    });
  }

  get Name() : string {
    return this.Pokemon.name;
  }

  get Sprite() : string {
    return this.Pokemon.sprites.front_default;
  }

  get Height() : number {
    return this.Pokemon.height;
  }

  get Weight() : number {
    return this.Pokemon.weight;
  }

  get IsCollected() : boolean {
    return this.pokemonCollectionService.contains(this.Pokemon);
  }

  ngOnInit(): void {
  }

  OnCollect() {
    this.pokemonCollectionService.add(this.Pokemon);
  }

  OnRemove() {
    this.pokemonCollectionService.remove(this.Pokemon);
  }
}
