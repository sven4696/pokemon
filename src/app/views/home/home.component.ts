import { Component, OnInit } from '@angular/core';
//import { HttpClient } from '@angular/common/http';
//import { Observable } from 'rxjs';

import { PokemonApiService } from '../../services/pokemon.api.service';
import { Pokemon, PokemonList, PokemonShort, PokemonSprite } from 'src/app/models/app.pokemon';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  pokemons: Array<Pokemon> = [];

  constructor(private pokemonAPI : PokemonApiService)
  {
    let items: Array<Pokemon> = [];
    pokemonAPI.getPokemons()
    .subscribe(pokemons => {
      pokemons.results.forEach(pokemon => {
        let item: Pokemon = this.createPokemon(pokemon);
        items.push(item);
      });

      items.forEach(item => {
        this.pokemons.push(item);
      })
    });
  }

  private createPokemon(pokeData: PokemonShort) : Pokemon
  {
    let splitUrl = pokeData.url.split('/');
    let id = Number(splitUrl[splitUrl.length - 2]);

    let pokemon: Pokemon = {
      url: pokeData.url,
      id: id,
      sprites: {
        front_default: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ id }.png`,
        back_default: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/${ id }.png`
      },
      name: pokeData.name,
    };

    return pokemon
  }

  ngOnInit(): void {
  }

}
