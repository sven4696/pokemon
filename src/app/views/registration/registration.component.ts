import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { UsernameValidator } from 'src/app/validators/username.validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  public registrationForm: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(20),
      UsernameValidator.cannotContainSpace
    ])
  });

  constructor(private authService: AuthenticationService, private router: Router) {
    if (authService.isAuthenticated()) {
      router.navigateByUrl('/catalogue');
    }
  }

  get username(): AbstractControl {
    return this.registrationForm.get('username');
  }

  get errorMessage() : string {
    if (this.username.hasError('required')) {
      return "Username is required";
    }

    if (this.username.hasError('minlength')) {
      return "Username must be greater than 2 characters";
    }

    if (this.username.hasError('cannotContainSpace')) {
      return "Username cannot contain spaces";
    }

    return this.username.hasError('maxlength') ? 'Username cannot be greater than 14 characters' : '';
  }

  onRegister() {
    this.authService.register(this.username.value);
    this.router.navigateByUrl('/catalogue');
  }

  ngOnInit(): void {
  }

}
